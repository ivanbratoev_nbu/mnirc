package org.mnirc.client;

import java.time.LocalDateTime;
import java.util.Set;

/**
 * Позволява получаването промени върху състоянието на данните на сървъра
 */
public interface MnircStateListener {
    /**
     * Извиква се щом се получат от сървъра съобщения, които не са налични в локалния кеш.
     *
     * @param from    изпращач
     * @param time    време на получаване (местна времева зона)
     * @param message тяло на съобщението
     */
    void onNewMessageReceived(String from, LocalDateTime time, String message);

    /**
     * Извиква се при промяна по множеството на никовете
     * @param nicks цялото множество на никовете в най-новия си вариант
     */
    void onNewNicksSet(Set<String> nicks);
}
