package org.mnirc.client;

import org.mnirc.NickExistsException;

import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * Основен прозорец на графичния интерфейс на клиента. Използва awt
 */
public class MnircClient extends Frame {
    /**
     * Списъкът с никове, които са вътре в чата
     */
    private List nicks;
    /**
     * Получените съобщения
     */
    private TextArea messages;
    /**
     * Новото съобщение, което ще бъде пратено
     */
    private TextArea input;
    /**
     * Отписване
     */
    private MenuItem exitMi;
    /**
     * Извикване на диалога за настройки
     */
    private MenuItem settingsMi;

    /**
     * Конструктор на графичния интерфейс на клиента. Създава основния прозорец и показва диалога за свързване
     */
    public MnircClient() {
        setSize(800, 800);
        setLayout(new BorderLayout());
        setExtendedState(Frame.MAXIMIZED_BOTH);

        nicks = new List();
        nicks.setEnabled(false);
        add(nicks, BorderLayout.EAST);

        Panel panel = new Panel();
        panel.setLayout(new BorderLayout());

        messages = new TextArea();
        messages.setEditable(false);
        panel.add(messages, BorderLayout.CENTER);

        input = new TextArea();
        panel.add(input, BorderLayout.SOUTH);

        add(panel, BorderLayout.CENTER);

        MenuBar mb = new MenuBar();
        Menu menu = new Menu("Menu");
        settingsMi = new MenuItem("Settings");
        exitMi = new MenuItem("Exit");
        menu.add(settingsMi);
        menu.add(exitMi);
        mb.add(menu);
        setMenuBar(mb);
        setVisible(true);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                System.exit(0);
            }
        });
        ConnectDialog connectDialog = new ConnectDialog(this);
        connectDialog.setVisible(true);
    }

    /**
     * Свързва този клиент към сървъра използвайки {@link MnircConnector}.
     * Имплементира {@link MnircStateListener} за този клиент и регистрира hook при спиране на програмата да се извика
     * {@link MnircConnector#exit()}
     *
     * @param nick    ник име, с което потребителят да се впише
     * @param address адрес към сървъра, без порт и протокол, например localhost за rmi://localhost:6969
     * @throws RemoteException       върнато от {@link MnircConnector}
     * @throws MalformedURLException върнато от {@link MnircConnector}
     * @throws NickExistsException   върнато от {@link MnircConnector}
     * @throws NotBoundException     върнато от {@link MnircConnector}
     * @see MnircConnector
     */
    private void connect(String nick, String address) throws RemoteException, MalformedURLException, NickExistsException, NotBoundException {
        MnircConnector connector = new MnircConnector(nick, address, new MnircStateListener() {
            @Override
            public void onNewMessageReceived(String from, LocalDateTime time, String message) {
                messages.append("[" + time.toString() + "]  " + from + " :: " + message + "\n");
                messages.setCaretPosition(messages.getText().length());
            }

            @Override
            public void onNewNicksSet(Set<String> _nicks) {
                nicks.removeAll();
                for (String n : _nicks) {
                    nicks.add(n);
                }
            }
        });

        settingsMi.addActionListener(e -> {
            SettingsDialog settingsDialog = new SettingsDialog(this, connector);
            settingsDialog.setVisible(true);
        });

        exitMi.addActionListener(e -> {
            try {
                connector.exit();
                messages.setText("");
                nicks.removeAll();
                ConnectDialog aboutDialog = new ConnectDialog(this);
                aboutDialog.setVisible(true);
            } catch (RemoteException e1) {
                e1.printStackTrace();
            }
        });

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            //noinspection ConstantConditions
            if (connector != null) {
                try {
                    connector.exit();
                } catch (RemoteException e) {
                    e.printStackTrace();
                }
            }
        }));
        input.addKeyListener(new KeyAdapter() {

            @Override
            public void keyReleased(KeyEvent e) {
                if (!e.isControlDown() && e.getKeyCode() == KeyEvent.VK_ENTER) {
                    try {
                        connector.sendMessage(input.getText());
                        input.setText("");
                    } catch (RemoteException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    super.keyReleased(e);
                }
            }
        });
    }

    /**
     * Диалог за въвеждане на ник и адрес за свързване
     */
    private class ConnectDialog extends Dialog {

        /**
         * Конструктор, който да изчертае диалога
         * @param owner {@inheritDoc}
         */
        ConnectDialog(Frame owner) {
            super(owner);
            setSize(400, 140);
            setLayout(new BorderLayout());
            Panel forms = new Panel();
            forms.setLayout(new GridLayout(2, 2));
            add(forms, BorderLayout.CENTER);
            Label nickLabel = new Label("Nickname: ");
            TextField nick = new TextField(30);
            forms.add(nickLabel);
            forms.add(nick);

            Label addressLabel = new Label("URI: ");
            TextField address = new TextField(30);
            forms.add(addressLabel);
            forms.add(address);

            Panel connectPanel = new Panel();
            Button connect = new Button("connect");
            connectPanel.add(connect);
            add(connectPanel, BorderLayout.SOUTH);


            final Toolkit toolkit = Toolkit.getDefaultToolkit();
            final Dimension screenSize = toolkit.getScreenSize();
            final int x = (screenSize.width - getWidth()) / 2;
            final int y = (screenSize.height - getHeight()) / 2;
            setLocation(x, y);

            KeyAdapter keyAdapter = new KeyAdapter() {
                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            connect(nick.getText(), address.getText());
                            dispose();
                        } catch (RemoteException | MalformedURLException | NickExistsException | NotBoundException e1) {
                            e1.printStackTrace();
                        }
                    } else {
                        super.keyPressed(e);
                    }

                }
            };
            this.addKeyListener(keyAdapter);
            nick.addKeyListener(keyAdapter);
            address.addKeyListener(keyAdapter);
            connect.addKeyListener(keyAdapter);
            connect.addActionListener(e -> {
                try {
                    connect(nick.getText(), address.getText());
                    this.dispose();
                } catch (RemoteException | MalformedURLException | NickExistsException | NotBoundException e1) {
                    e1.printStackTrace();
                }
            });

            addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent windowEvent) {
                    System.exit(0);
                }
            });
        }

    }

    /**
     * Диалог за настройки на клиента. Позволява да бъде зададен лимит ({@link MnircConnector#limit})
     * и период на опресняване на съобщенията ({@link MnircConnector#sleepMillis})
     */
    private class SettingsDialog extends Dialog {

        /**
         * Конструктор, който да изчертае диалога
         * @param owner {@inheritDoc}
         * @param connector конекторът, чиито настойки ще бъдат променяни
         * @see MnircConnector
         */
        SettingsDialog(Frame owner, MnircConnector connector) {
            super(owner);
            setSize(400, 140);
            setLayout(new BorderLayout());
            Panel forms = new Panel();
            forms.setLayout(new GridLayout(2, 2));
            add(forms, BorderLayout.CENTER);
            Label limitLabel = new Label("Message cache limit: ");
            TextField limit = new TextField(30);
            limit.setText(String.valueOf(connector.getLimit()));
            forms.add(limitLabel);
            forms.add(limit);

            Label sleepMillisLabel = new Label("Message update interval: ");
            TextField sleepMillis = new TextField(30);
            sleepMillis.setText(String.valueOf(connector.getSleepMillis()));
            forms.add(sleepMillisLabel);
            forms.add(sleepMillis);

            Panel submitPanel = new Panel();
            Button submit = new Button("Ok");
            submitPanel.add(submit);
            add(submitPanel, BorderLayout.SOUTH);

            submit.addActionListener(e -> {
                connector.setLimit(Long.parseLong(limit.getText()));
                connector.setSleepMillis(Long.parseLong(sleepMillis.getText()));
                dispose();
            });

            final Toolkit toolkit = Toolkit.getDefaultToolkit();
            final Dimension screenSize = toolkit.getScreenSize();
            final int x = (screenSize.width - getWidth()) / 2;
            final int y = (screenSize.height - getHeight()) / 2;
            setLocation(x, y);

            addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    dispose();
                }
            });
        }
    }
}
