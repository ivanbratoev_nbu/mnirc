package org.mnirc.client;

import org.mnirc.GetMessagesOutput;
import org.mnirc.Message;
import org.mnirc.NickExistsException;
import org.mnirc.RMIInterface;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Клас, който служи да връзка с отдалечения сървър. Обвива {@link RMIInterface} и предоставя абстракция около всички
 * конвертирания между времеви зони, държи информацията за nick и messages, предоставя запитването за съобщения.
 * Предоставя новата информация за никове и съобщения като вика методите на {@link MnircStateListener}
 *
 * @see MnircStateListener
 */
public class MnircConnector {
    /**
     * RMI интерфейсът с отдалечения сървър
     */
    private final RMIInterface remote;
    /**
     * Ник на влезлият потребител
     */
    private final String nick;
    /**
     * Множеството на никовете, влезли в момента в чата. Кеш от сървъра.
     */
    private final SortedSet<String> nicks;
    /**
     * Множеството на получените съобщения. Използва се за да се се проверяват съобщенията, получени от сървъра,
     * дали са нови (не участват в това множество) или не
     */
    private final SortedSet<Message> messages;
    /**
     * обектът, който слуша за промени в множеството на никовете и нови съобщения
     */
    private final MnircStateListener listener;
    /**
     * ограничение на максималния брой съобщения, които да се пазят в {@link MnircConnector#messages}
     */
    private AtomicLong limit;
    /**
     * брой милисекунди между заявките към {@link RMIInterface#getMessages(ZonedDateTime, long)}
     */
    private AtomicLong sleepMillis;
    /**
     * Указва дали конекторът работи. Ако се маркира, че не работи, цикълът за проверяване за нови съобщения ще приключи
     */
    private AtomicBoolean running;
    /**
     * Време, когато съобщение е получено за последен път
     */
    private ZonedDateTime lastReceived;

    /**
     * Конструктор за конектора. Свързва се с отдалечения сървър и стартира проверките за нови съобщения
     * @param nick ник, с който потребителят да се впише
     * @param serverUrl адрес към сървъра, без порт и протокол, например localhost за rmi://localhost:6969
     * @param listener обект, слушащ за промени по множеството от никове и нови съобщения
     * @throws RemoteException  в случай, че има проблем с достъпването на сървъра
     * @throws NotBoundException връщан от {@link Naming#lookup(String)}
     * @throws MalformedURLException връщан от {@link Naming#lookup(String)}
     * @throws NickExistsException връщан от {@link RMIInterface#enter(String)}
     * @see Naming#lookup(String)
     */
    MnircConnector(String nick, String serverUrl, MnircStateListener listener)
            throws RemoteException, NotBoundException, MalformedURLException, NickExistsException {
        this.remote = (RMIInterface) Naming.lookup("rmi://" + serverUrl + ":6969/mnirc");
        this.nick = nick;
        this.nicks = new TreeSet<>();
        this.remote.enter(nick);
        this.messages = new TreeSet<>();
        this.limit = new AtomicLong(100);
        this.sleepMillis = new AtomicLong(100);
        this.listener = listener;
        this.running = new AtomicBoolean(true);
        this.lastReceived = ZonedDateTime.of(0, 1, 1, 0, 0, 0, 0, ZoneOffset.UTC);
        new MessagePoller().start();
    }

    /**
     * Изпраща съобщение на сървъра
     * @param message съобщение, което да бъде изпратено на сървъра
     * @throws RemoteException  в случай, че има проблем с достъпването на сървъра
     * @see Message
     */
    public void sendMessage(String message) throws RemoteException {
        ZonedDateTime now = ZonedDateTime.now(ZoneOffset.UTC);
        this.remote.sendMessage(new Message(this.nick, message, now));
    }

    /**
     * Съобщава на сървъра, че този клиент излиза от чата
     * @throws RemoteException  в случай, че има проблем с достъпването на сървъра
     */
    public void exit() throws RemoteException {
        this.remote.exit(this.nick);
        this.running.set(false);
    }

    /**
     * @return ограничението за максимален брой съобщения в кеша
     * @see MnircConnector#limit
     */
    public long getLimit() {
        return this.limit.get();
    }

    /**
     *
     * @param limit нова стойност за ограничението за максимален брой съобщения в кеша
     * @see MnircConnector#limit
     */
    public void setLimit(long limit) {
        this.limit.set(limit);
    }

    /**
     * @return Времето, през което се опресняват съобщенията
     * @see MnircConnector#sleepMillis
     */
    public long getSleepMillis() {
        return sleepMillis.get();
    }

    /**
     * @param sleepMillis нова стойност на времето, през което се опресняват съобщенията
     * @see MnircConnector#sleepMillis
     */
    public void setSleepMillis(long sleepMillis) {
        this.sleepMillis.set(sleepMillis);
    }

    /**
     * Нишка, която реализира получаването на съобщения от сървъра.
     * Докато {@link MnircConnector#running} е вярно, вика {@link RMIInterface#getMessages(ZonedDateTime, long)}
     * през период {@link MnircConnector#sleepMillis}
     * При нови данни вика съответните методи в {@link MnircStateListener}
     */
    private class MessagePoller extends Thread {
        @Override
        public void run() {
            while (running.get()) {
                try {
                    GetMessagesOutput output = remote.getMessages(lastReceived, limit.get());

                    boolean callNicksListener = false;
                    synchronized (nicks) {
                        if (!nicks.containsAll(output.nicks)) {
                            nicks.addAll(output.nicks);
                            callNicksListener = true;
                        }
                    }
                    if (callNicksListener) {
                        listener.onNewNicksSet(nicks);
                    }

                    List<Message> newMessages = new ArrayList<>();
                    synchronized (messages) {
                        for (Message msg : output.messages) {
                            if (!messages.contains(msg)) {
                                newMessages.add(msg);
                            }
                        }
                        messages.addAll(output.messages);
                        while (messages.size() > limit.get()) {
                            messages.remove(messages.first());
                        }
                    }
                    for (Message msg : newMessages) {
                        String from = msg.from;
                        String message = msg.message;
                        ZonedDateTime receivedAtZoned = msg.receivedAt;
                        LocalDateTime receivedAt = receivedAtZoned.toLocalDateTime();
                        listener.onNewMessageReceived(from, receivedAt, message);
                    }
                } catch (RemoteException e) {
                    e.printStackTrace();
                }

                try {
                    Thread.sleep(sleepMillis.get());
                } catch (InterruptedException ignored) {
                }
            }
        }
    }
}

