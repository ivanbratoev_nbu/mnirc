package org.mnirc.server;

import org.mnirc.GetMessagesOutput;
import org.mnirc.Message;
import org.mnirc.NickExistsException;
import org.mnirc.RMIInterface;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.ZonedDateTime;
import java.util.*;

/**
 * Имплементация на {@link RMIInterface} , работеща на отдалечения RMI сървър
 */
@SuppressWarnings("RedundantThrows")
public class MnircServer extends UnicastRemoteObject implements RMIInterface {

    /**
     * всички регистрирани съобщения
     */
    public final List<Message> messages;
    /**
     * горна граница на размера на {@link MnircServer#messages}
     */
    private final long totalLimit;
    /**
     * В случай, че бъде получено ново съобщение и {@link MnircServer#messages} е пълен спрямо ограничението си
     * {@link MnircServer#totalLimit} ще бъдат изтрити толкова на брой най-стари съобщения
     */
    private final long cleanStep;
    /**
     * Множеството на никовете, които са в чата в момента
     */
    private final Set<String> nicks;


    /**
     * Конструктор на сървъра
     *
     * @param totalLimit горна граница на размера на {@link MnircServer#messages}
     * @param cleanStep  * В случай, че бъде получено ново съобщение и {@link MnircServer#messages} е пълен спрямо ограничението си
     *                   {@link MnircServer#totalLimit} ще бъдат изтрити толкова на брой най-стари съобщения
     * @param initial    съобщения, вземи от локалния кеш на диска при стартиране
     * @throws RemoteException стандартна RMI грешка
     */
    public MnircServer(long totalLimit, long cleanStep, List<Message> initial) throws RemoteException {
        super();
        this.totalLimit = totalLimit;
        this.cleanStep = cleanStep;
        this.messages = initial;
        this.nicks = new HashSet<>();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetMessagesOutput getMessages(ZonedDateTime lastReceived, long limit) throws RemoteException {
        Stack<Message> stack = new Stack<>();
        stack.ensureCapacity((int) limit);
        synchronized (this.messages) {
            ListIterator li = this.messages.listIterator(this.messages.size());
            while (li.hasPrevious() && stack.size() < limit) {
                Message msg = (Message) li.previous();
                if (msg.receivedAt.isAfter(lastReceived)) {
                    stack.push(msg);
                } else {
                    break;
                }
            }
        }
        Set<String> nicks;
        synchronized (this.nicks) {
            nicks = new HashSet<>(this.nicks);
        }
        SortedSet<Message> msgs = new TreeSet<>();
        while (!stack.isEmpty()) {
            msgs.add(stack.pop());
        }

        return new GetMessagesOutput(nicks, msgs);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enter(String nick) throws RemoteException, NickExistsException {
        synchronized (this.nicks) {
            if (this.nicks.contains(nick)) {
                throw new NickExistsException();
            }
            this.nicks.add(nick);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void exit(String nick) throws RemoteException {
        synchronized (this.nicks) {
            this.nicks.remove(nick);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sendMessage(Message message) throws RemoteException {
        synchronized (this.messages) {
            if (this.messages.size() >= this.totalLimit) {
                for (long c = 0; c < this.cleanStep; ++c) {
                    this.messages.remove(0);
                }
            }
            this.messages.add(message);
        }
    }
}
