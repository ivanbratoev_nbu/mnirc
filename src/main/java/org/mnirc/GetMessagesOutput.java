package org.mnirc;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Set;
import java.util.SortedSet;

/**
 * Този клас служи за да обвие никовете и съобщенията, върнати от {@link org.mnirc.RMIInterface#getMessages(ZonedDateTime, long)} .
 * Тъй, като се предава през RMI, този клас имплементира {@link Serializable} .
 */
public class GetMessagesOutput implements Serializable {
    /**
     * Множество от никове
     *
     * @see Set
     */
    public final Set<String> nicks;
    /**
     * Подредено множество от съобщения
     * @see SortedSet
     * @see Message
     */
    public final SortedSet<Message> messages;

    /**
     * Конструктор, позволяващ единствен immutable начин за работа с този клас (полетата са final)
     * @param nicks множество от никове
     * @param messages подредено множество от съобщения
     * @see Set
     * @see SortedSet
     * @see Message
     */
    public GetMessagesOutput(Set<String> nicks, SortedSet<Message> messages) {
        this.nicks = nicks;
        this.messages = messages;
    }
}
