package org.mnirc;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Този клас служи за представяне на съобщение. Имплементира {@link Serializable} за да бъде пренасян през RMI.
 * Имплементира {@link Comparable} за да може да бъде съхраняван в {@link java.util.Set} и {@link java.util.SortedSet}
 */
public class Message implements Serializable, Comparable {
    /**
     * Изпращач на съобщението
     */
    public final String from;
    /**
     * Тяло на съобщението
     */
    public final String message;
    /**
     * Време на приемане на съобщението в UTC
     */
    public final ZonedDateTime receivedAt;

    /**
     * Конструктор, позволяващ единствен immutable начин за работа с този клас (полетата са final)
     *
     * @param from       ник на потребителя, изпратил/изпращащ съобщението
     * @param message    самото съобщение
     * @param receivedAt време, в което съобщението е изпратено. Забележка: това време е в UTC.
     */
    public Message(String from, String message, ZonedDateTime receivedAt) {
        this.from = from;
        this.message = message;
        this.receivedAt = receivedAt;
    }

    /**
     * Конструктор за десериализация от JSON
     * @param obj JSON обект, описващ Message обект
     * @return десериализиран Message обект
     */
    private static Message fromJson(JSONObject obj) {
        String from = (String) obj.get("from");
        String message = (String) obj.get("message");
        String recvAtStr = (String) obj.get("recvAt");
        ZonedDateTime recvAt = ZonedDateTime.parse(recvAtStr);
        return new Message(from, message, recvAt);
    }

    /**
     * Метод за сериализация на списък от съобщения в JSON.
     * @param messages списъкът от съобщения
     * @return JSON масив в символен низ
     */
    @SuppressWarnings("unchecked")
    public static String messageListToJson(List<Message> messages) {
        JSONArray arr = new JSONArray();
        for (Message msg : messages) {
            arr.add(msg.toJson());
        }
        return arr.toJSONString();
    }

    /**
     * Метод за десериализация на списък от съобщения от JSON
     * @param json JSON масив от обекти, описващи Message обекти
     * @return десериализираният списък
     */
    public static List<Message> messageListFromJson(String json) {
        List<Message> messages = new ArrayList<>();
        try {
            JSONParser parser = new JSONParser();
            Object object = parser.parse(json);
            JSONArray array = (JSONArray) object;
            for (Object obj : array) {
                messages.add(Message.fromJson((JSONObject) obj));
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return messages;
    }

    /**
     * Сериализира Message обектът в JSON обект
     * @return сериализираният обект
     */
    @SuppressWarnings("unchecked")
    private JSONObject toJson() {
        String recvAtStr = receivedAt.toString();
        JSONObject obj = new JSONObject();
        obj.put("from", from);
        obj.put("message", message);
        obj.put("recvAt", recvAtStr);
        return obj;
    }

    /**
     * Методът за сравнение на Message обекти. Нужен ни е за да можем да указваме уникалност и подредба на Message обекти.
     * За да бъде това изпълнено вярно сравнението се изпълнява на следните етапи:
     * <ul>
     *     <li>Най-важно е кога е получено съобщението. Ако времената съвпадат е слабо вероятно обектите да са различни, но в случай на силно зает сървър е редно да проверим другите полета</li>
     *     <li>Проверяваме съобщението. До тук редът няма голямо значение</li>
     *     <li>Сравняваме и ника</li>
     * </ul>
     * @param o другото съобщение за сравенение
     * @return {@literal < 0} по-малко, {@literal > 0} по-голямо, 0 = еквивалентност
     */
    @Override
    public int compareTo(Object o) {
        Message other = (Message) o;
        int res = this.receivedAt.compareTo(other.receivedAt);
        if (res != 0) {
            return res;
        }
        int resM = this.message.compareTo(other.message);
        if (resM != 0) {
            return res;
        }
        return this.from.compareTo(other.from);
    }
}
