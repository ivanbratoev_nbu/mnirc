package org.mnirc;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.time.ZonedDateTime;

/***
 * Интерфейс между RMI клиент и сървър. Описва методите, които сървъра предоставя за клиентите
 * @see Remote
 */
public interface RMIInterface extends Remote {
    /**
     * Връща всички съобщения, налични на сървъра, получени след lastReceived
     *
     * @param lastReceived моментът от време, в който за последно е получено съобщение от сървъра. Методът ще върне единствено съобщения по-нови от това.
     *                     Забележка: това време трябва да е в UTC!
     * @param limit        максимален брой съобщения, които да бъдат върнати. Ако съобщенията по-нови от lastReceived са повече
     *                     от limit, ще бъдат върнати последните limit на брой съобщения
     * @return {@link GetMessagesOutput}
     * @throws RemoteException в случай, че има проблем с достъпването на сървъра от клиента
     * @see GetMessagesOutput
     * @see ZonedDateTime
     */
    GetMessagesOutput getMessages(ZonedDateTime lastReceived, long limit) throws RemoteException;

    /**
     * Регистрира потребител с ник nick в сървъра
     * @param nick ник-име
     * @throws RemoteException  в случай, че има проблем с достъпването на сървъра от клиента
     * @throws NickExistsException в случай, че nick вече се използва на сървъра
     * Забележка: важно е, при излизане на клиента, да се извика {@link RMIInterface#exit(String)}
     * @see NickExistsException
     */
    void enter(String nick) throws RemoteException, NickExistsException;

    /**
     * Съобщава на сървъра, че потребителят с ник nick излиза от чата и трябва да бъде изтрит от списъка с никове
     * @param nick никът, който излиза от чата
     * @throws RemoteException  в случай, че има проблем с достъпването на сървъра от клиента
     */
    void exit(String nick) throws RemoteException;

    /**
     * Изпраща съобщение на сървъра
     * @param message съобщението
     * @throws RemoteException  в случай, че има проблем с достъпването на сървъра от клиента
     * @see Message
     */
    void sendMessage(Message message) throws RemoteException;
}
