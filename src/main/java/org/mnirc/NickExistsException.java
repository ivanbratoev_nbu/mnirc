package org.mnirc;

/**
 * Изключение, което се връща от {@link RMIInterface#enter(String)} в случай, че никът, зададен като 1ви аргумент на
 * {@link RMIInterface#enter(String)} вече е използван на сървъра
 */
public class NickExistsException extends Exception {
    public NickExistsException() {
        super();
    }
}
