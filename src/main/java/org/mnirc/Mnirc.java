package org.mnirc;

import org.mnirc.client.MnircClient;
import org.mnirc.server.MnircServer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;

/**
 * Входен клас за програмата
 */
public class Mnirc {

    private static final String USAGE = "Usage: <cmd> where cmd = server, client";

    /**
     * Вход на програмата. Приема един аргумент, който може да бъде server или client
     *
     * @param args 1 аргумент - server или client според това коя част от програмата се стартира
     */
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println(USAGE);
        }
        String cmd = args[0];
        switch (cmd) {
            case "server":
                setupServer();
                break;
            case "client":
                setupClient();
                break;
            default:
                System.err.println(USAGE);
                break;
        }
    }

    /**
     * Настройка на сървърната част. Чете настройки/кеш от $HOME/.config/mnirc_server и стартира RMI сървъра на порт 6969.
     * При получаване на сигнал за спиране записва настройките и кеша преди програмата да прекрати работа.
     */
    private static void setupServer() {
        try {
            String confPath = Paths.get(System.getProperty("user.home"), ".config", "mnirc_server").toAbsolutePath().toString();
            File yourFile = new File(confPath);
            //noinspection ResultOfMethodCallIgnored
            yourFile.getParentFile().mkdirs();
            //noinspection ResultOfMethodCallIgnored
            yourFile.createNewFile();
            Properties props = new Properties();
            FileInputStream in = new FileInputStream(confPath);
            props.load(in);
            in.close();

            String totalLimitStr = props.getProperty("totalLimit", "1000");
            props.setProperty("totalLimit", totalLimitStr);
            long totalLimit = Long.parseLong(totalLimitStr);

            String cleanStepStr = props.getProperty("cleanStep", "50");
            props.setProperty("cleanStep", cleanStepStr);
            long cleanStep = Long.parseLong(cleanStepStr);

            String messagesStr = props.getProperty("messages", "[]");
            props.setProperty("messages", messagesStr);
            List<Message> messages = Message.messageListFromJson(messagesStr);

            MnircServer server = new MnircServer(totalLimit, cleanStep, messages);

            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                try {
                    List<Message> messageList = server.messages;
                    String messageListStr = Message.messageListToJson(messageList);
                    props.setProperty("messages", messageListStr);
                    FileOutputStream out = new FileOutputStream(confPath);
                    props.store(out, "");
                    out.close();
                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }
            }));


            System.setProperty("java.security.policy", "file:./security.policy");
            //noinspection deprecation
            System.setSecurityManager(new java.rmi.RMISecurityManager());
            java.rmi.registry.LocateRegistry.createRegistry(6969);
            java.rmi.Naming.rebind("//:6969/mnirc", server);
            System.out.println("Server ready");

        } catch (Exception e) {
            System.err.println("Server exception: " + e.toString());
            e.printStackTrace();
        }
    }

    /**
     * Настройка на клиента. Просто го изчертава на екрана.
     */
    private static void setupClient() {
        new MnircClient();
    }
}
